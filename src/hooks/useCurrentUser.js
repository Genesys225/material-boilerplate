import { useEffect, useState } from 'react';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from 'src/config/firebase';

export default function useCurrentUser() {
  const [firebaseUser] = useAuthState(auth);
  const [user, setUser] = useState(false);

  useEffect(() => {
    if (firebaseUser) {
      setUser({
        name: firebaseUser.displayName,
        avatar: firebaseUser.photoURL,
        email: firebaseUser.email
      });
    }
  }, [firebaseUser]);

  return user;
}
