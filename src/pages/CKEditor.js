import { Helmet } from 'react-helmet';
import { Box, Container } from '@material-ui/core';

import CKEditor from 'src/components/CKeditor/CKEditor';

const CKEditorPage = () => (
  <>
    <Helmet>
      <title>Customers | Material Kit</title>
    </Helmet>
    <Box
      sx={{
        backgroundColor: 'background.default',
        minHeight: '100%',
        py: 3
      }}
    >
      <Container maxWidth={false}>
        <Box sx={{ pt: 3 }}>
          <CKEditor />
        </Box>
      </Container>
    </Box>
  </>
);

export default CKEditorPage;
