import { Navigate } from 'react-router-dom';
import DashboardLayout from 'src/components/DashboardLayout';
import MainLayout from 'src/components/MainLayout';
import Account from 'src/pages/Account';
import CKEditorPage from 'src/pages/CKEditor';
import Dashboard from 'src/pages/Dashboard';
import Login from 'src/pages/Login';
import NotFound from 'src/pages/NotFound';
import ProductList from 'src/pages/ProductList';
import Register from 'src/pages/Register';
import Settings from 'src/pages/Settings';
import CustomerList from 'src/pages/CustomerList';
import { useAuthState } from 'react-firebase-hooks/auth';
import { auth } from './config/firebase';

const routes = (user) => {
  const [firebaseUser] = useAuthState(auth);

  return [
    {
      path: 'app',
      element: <DashboardLayout user={user} />,
      children: [
        { path: 'account', element: <Account /> },
        { path: 'customers', element: <CustomerList /> },
        { path: 'dashboard', element: <Dashboard /> },
        { path: 'products', element: <ProductList /> },
        { path: 'settings', element: <Settings /> },
        { path: 'ck-editor', element: <CKEditorPage /> },
        { path: '*', element: <Navigate to="/404" /> }
      ]
    },
    {
      path: '/',
      element: <MainLayout />,
      children: [
        { path: 'login', element: firebaseUser ? <Navigate to="/app/dashboard" /> : <Login /> },
        { path: 'register', element: <Register /> },
        { path: '404', element: <NotFound /> },
        { path: '/', element: <Navigate to="/app/dashboard" /> },
        { path: '*', element: <Navigate to="/404" /> }
      ]
    }
  ];
};

export default routes;
