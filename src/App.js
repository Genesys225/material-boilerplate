/* eslint-disable no-unused-vars */
import 'react-perfect-scrollbar/dist/css/styles.css';
import { useRoutes } from 'react-router-dom';
import { ThemeProvider } from '@material-ui/core';
import GlobalStyles from 'src/components/GlobalStyles';
import 'src/mixins/chartjs';
import theme from 'src/theme';
import routes from 'src/routes';
import { useAuthState } from 'react-firebase-hooks/auth';
import { useCollectionData } from 'react-firebase-hooks/firestore';
import { useEffect, useState } from 'react';
import { auth, firestore } from './config/firebase';

const App = () => {
  const [firebaseUser] = useAuthState(auth);
  const [user, setUser] = useState(false);
  const routing = useRoutes(routes(user));

  useEffect(() => {
    if (firebaseUser) {
      setUser({
        name: firebaseUser.displayName,
        avatar: firebaseUser.photoURL,
        email: firebaseUser.email
      });
    }
  }, [firebaseUser]);

  return (
    <ThemeProvider theme={theme}>
      <GlobalStyles />
      {routing}
    </ThemeProvider>
  );
};

export default App;
