## Material Kit - React

![license](https://img.shields.io/badge/license-MIT-blue.svg)


> Free React Admin Dashboard made with [Material UI's](https://material-ui.com/?ref=devias-io) components, [React](https://reactjs.org/?ref=devias-io) and of course [create-react-app](https://facebook.github.io/create-react-app/?ref=devias-io) to boost your app development process! We'll launch a pro version soon, so if you are interested subscribe to our personal emailing list on [https://devias.io/](https://devias.io/)



## Quick start

- [Download from Github](https://github.com/devias-io/material-kit-react/archive/master.zip) or [Download from Devias](https://devias.io/products/material-kit-react) or clone the repo: `git clone https://github.com/devias-io/material-kit-react.git`

- Make sure your NodeJS and npm versions are up to date for `React 17`

- Install dependencies: `npm install` or `yarn`

- Start the server: `npm run start` or `yarn start`

- Views are on: `localhost:3000`

## 🖌 Design Files

👉[Download Sketch file](https://s3.eu-west-2.amazonaws.com/devias/products/react-material-dashboard/react-material-dashboard-free.sketch)

👉[Download Figma file](https://devias.s3.eu-west-2.amazonaws.com/products/react-material-dashboard/react-material-dashboard-free.fig)

## File Structure

Within the download you'll find the following directories and files:

```
material-react-dashboard

├── .eslintrc
├── .gitignore
├── .prettierrc
├── CHANGELOG.md
├── jsconfig.json
├── LICENSE.md
├── package.json
├── README.md
├── public
├── docs
└── src
	├── assets
	├── common
	├── components
	├── helpers
	├── icons
	├── layouts
	├── theme
	├── views
	│	├── Account
	│	├── Dashboard
	│	├── Icons
	│	├── NotFound
	│	├── ProductList
	│	├── Settings
	│	├── SignIn
	│	├── SignUp
	│	├── Typography
	│	└── UserList
	├── App.jsx
	├── index.jsx
	└── Routes.jsx
```

## Resources



## Reporting Issues:

- [Github Issues Page](https://gitlab.com/Genesys225/material-boilerplate/-/issues/new)


